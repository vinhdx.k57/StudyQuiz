export default {
  enterText: 'Enter Text',
  enterSubText: 'Enter Subtext',
  submit: 'Submit',
  result: 'Result: ',
  back: 'Back',
  alert: 'Alert',
  tabs: {
    collection: 'Collection',
    preminum: 'Preminum',
    resource: 'Resource',
    setting: 'Setting',
    statistic: 'Statistic'
  }


}