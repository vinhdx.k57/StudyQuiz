/**
 * Tabs Scenes
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React from 'react';
import { Scene, Tabs, Stack, ActionConst } from 'react-native-router-flux';

// Consts and Libs
import { AppColors } from '@theme/';

import CollectionScene from './tabs/CollectionScene';
import PremiumScene from './tabs/PremiumScene';
import ResourceScene from './tabs/ResourceScene';
import SettingScene from './tabs/SettingScene';
import StatisticsScene from './tabs/StatisticsScene';


/* Routes ==================================================================== */
export default (
    <Tabs
        key='main'
        swipeEnabled={false}
        showLabel={false}
        tabBarPosition='bottom'
        wrap={false}
        activeBackgroundColor={AppColors.tabbar.background.inactive}
        inactiveBackgroundColor={AppColors.tabbar.background.active}
        lazy={true}
    >
        {CollectionScene}
        {PremiumScene}
        {ResourceScene}
        {StatisticsScene}
        {SettingScene}
    </Tabs>
);
