import React from 'react';
import { Scene, Tabs, Stack, ActionConst } from 'react-native-router-flux';


// Consts and Libs
import { Messages } from '@constant';
import { AppStyles, AppColors } from '@theme/';

// Scenes
import SettingScreen from '@container/setting/SettingScreen';


export default (
  <Stack key='SettingTab'
    title={Messages.tabs.setting}
    tabBarLabel={Messages.tabs.notification}
    inactiveBackgroundColor={AppColors.tabbar.background.inactive}
    activeBackgroundColor={AppColors.tabbar.background.active}
    navigationBarStyle={AppStyles.navbar}
    titleStyle={AppStyles.navbarTitle}
  >
    <Scene key='statistic' component={SettingScreen} />

  </Stack>
);