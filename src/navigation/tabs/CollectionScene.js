import React from 'react';
import { Scene, Tabs, Stack, ActionConst } from 'react-native-router-flux';


// Consts and Libs
import { Messages } from '@constant';
import { AppStyles, AppColors } from '@theme/';

// Scenes
import CollectionScreen from '@container/collection/CollectionScreen';


export default (
  <Stack key='CollectionTab'
    title={Messages.tabs.collection}
    tabBarLabel={Messages.tabs.notification}
    inactiveBackgroundColor={AppColors.tabbar.background.inactive}
    activeBackgroundColor={AppColors.tabbar.background.active}
    navigationBarStyle={AppStyles.navbar}
    titleStyle={AppStyles.navbarTitle}
  >
    <Scene key='collection' component={CollectionScreen} />

  </Stack>
);