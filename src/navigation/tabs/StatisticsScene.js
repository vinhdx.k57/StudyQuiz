import React from 'react';
import { Scene, Tabs, Stack, ActionConst } from 'react-native-router-flux';


// Consts and Libs
import { Messages } from '@constant';
import { AppStyles, AppColors } from '@theme/';

// Scenes
import StatiticsScreen from '@container/statistics/StatisticsScreen';


export default (
  <Stack key='StatisticTab'
    title={Messages.tabs.collection}
    tabBarLabel={Messages.tabs.statistics}
    inactiveBackgroundColor={AppColors.tabbar.background.inactive}
    activeBackgroundColor={AppColors.tabbar.background.active}
    navigationBarStyle={AppStyles.navbar}
    titleStyle={AppStyles.navbarTitle}
  >
    <Scene key='statistic' component={StatiticsScreen} />

  </Stack>
);