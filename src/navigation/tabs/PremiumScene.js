import React from 'react';
import { Scene, Tabs, Stack, ActionConst } from 'react-native-router-flux';


// Consts and Libs
import { Messages } from '@constant';
import { AppStyles, AppColors } from '@theme/';

// Scenes
import PremiumScreen from '@container/premium/PremiumScreen';


export default (
  <Stack key='PremiumTab'
    title={Messages.tabs.premium}
    tabBarLabel={Messages.tabs.resource}
    inactiveBackgroundColor={AppColors.tabbar.background.inactive}
    activeBackgroundColor={AppColors.tabbar.background.active}
    navigationBarStyle={AppStyles.navbar}
    titleStyle={AppStyles.navbarTitle}
  >
    <Scene key='statistic' component={PremiumScreen} />

  </Stack>
);