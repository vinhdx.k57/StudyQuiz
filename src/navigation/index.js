/**
 * App Navigation
 */
import React from 'react';
import { Actions, Scene, Stack, Router } from 'react-native-router-flux';

// Scenes
import SplashScreen from '@container/SplashScreen.js';
import UserProfileScreen from '@container/UserProfileScreen';
import MainScene from './main'

/* Routes ==================================================================== */
export default Actions.create(
  <Router>
    <Stack key="root" hideNavBar panHandlers={null}>
      {MainScene}
      <Scene key="splash" component={SplashScreen} />
      <Scene key="userProfile" component={UserProfileScreen} />
    </Stack>
  </Router>
);
